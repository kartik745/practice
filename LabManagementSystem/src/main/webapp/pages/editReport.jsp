<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
}

* {
	box-sizing: border-box;
}

input[type=text], select, textarea {
	width: 50%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
	margin-top: 6px;
	margin-bottom: 16px;
	resize: vertical;
}

input[type=submit] {
	background-color: #4CAF50;
	color: white;
	padding: 12px 20px;
	border: none;
	border-radius: 4px;
	cursor: pointer;
}

input[type=submit]:hover {
	background-color: #45a049;
}

.container {
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 20px;
}
</style>
</head>
<body>

	<div class="container">
		<form:form action="updateReport" modelAttribute="report" method="post">

			<label for="Report Name">Report Type</label>
			<form:input path="reportName" type="text" name="reportName"
				required="true" placeholder="report name.." />
			<br>
			<form:input type="hidden" path="reportId" name="reportId" />
			<label for="price">Price </label>
			<form:input type="number" name="price" path="price" required="true"
				placeholder="price" />

			<br>
			<input type="submit" value="Submit">

  </</form:form>
	</div>
</body>
</html>