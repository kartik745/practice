<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
}

* {
	box-sizing: border-box;
}

input[type=text], select {
	width: 50%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
	margin-top: 6px;
	margin-bottom: 16px;
	resize: vertical;
}

input[type=submit] {
	background-color: #4CAF50;
	color: white;
	padding: 12px 20px;
	border: none;
	border-radius: 4px;
	cursor: pointer;
}

input[type=submit]:hover {
	background-color: #45a049;
}

.container {
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 20px;
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script type="text/javascript">
	function fillCitydd() {
		var state = document.getElementById("state");
		var city = document.getElementById("city");
		while (city.hasChildNodes()) {
			city.removeChild(city.firstChild);
		}
		var htp = new XMLHttpRequest();
		htp.onreadystatechange = function() {
			if (htp.readyState == 4) {
				var json = JSON.parse(htp.responseText);
				for (var i = 0; i < json.length; i++) {
					var opt = document.createElement("option");
					opt.value = json[i].cityId;
					opt.innerHTML = json[i].cityName;
					city.options.add(opt);
				}
			}
		}
		htp.open("Get", "getCity?stateId=" + state.value, true);
		htp.send();
	}
</script>
<script>
	$(document).ready(function() {
		$("#report").click(function() {
			var value = $(this).val();
			$.ajax({
		        type: "GET",
		        url: "/calculateAmount/"+ value,
		        success: function (data) {
		                    console.log(data);
		                    $("#totalAmount").val(data)
		        }
		    });
		});
	});
</script>
</head>
<body>
	<div class="container">
		<form:form modelAttribute="appointment" action="saveAppointment"
			method="post">

    Patient Name:
    <form:input type="text" path="patientName" name="patientName"
				placeholder="patient name.." required="true" />
			<br>

    Email :
    <form:input type="text" path="email" name="Email"
				placeholder="email" required="true" />
			<br>
    
   Phone Number : 
    <form:input type="number" name="phoneNumber" path="phoneNumber"
				placeholder="Phone Number" required="true" />
			<br>

			Report :
			<form:select path="report" name="report1" required="true" id="report">
				<c:forEach items="${report}" var="i">
					<form:option id="reportId" name="report" value="${i.reportId}">${i.reportName} - ${i.price}</form:option>
				</c:forEach>
			</form:select>

			<br>
			
			
			State :
			<form:select id="state" path="state" name="state1" required="true"
				onchange="fillCitydd()">
				<c:forEach items="${state}" var="i">
					<form:option id="stateVal" name="state" value="${i.stateId}">${i.stateName}</form:option>
				</c:forEach>
			</form:select>
			<br>

			City:
			<form:select path="city" id="city" name="city" required="true">
				<form:option name="city" value=""></form:option>
			</form:select>
			
		<br>
		<br>
   Total Amount : 
    <form:input type="number" id="totalAmount" name="totalAmount"
				path="totalAmount" placeholder="Total Amount" />
			<br>
			<input type="submit" value="Submit">

		</form:form>
	</div>
</body>
</html>