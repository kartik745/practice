<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<link
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
    crossorigin="anonymous">
<meta charset="ISO-8859-1">
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
}

* {
	box-sizing: border-box;
}

input[type=text], select {
	width: 50%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
	margin-top: 6px;
	margin-bottom: 16px;
	resize: vertical;
}

input[type=submit] {
	background-color: #4CAF50;
	color: white;
	padding: 12px 20px;
	border: none;
	border-radius: 4px;
	cursor: pointer;
}

input[type=submit]:hover {
	background-color: #45a049;
}

.container {
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 20px;
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	
	$(document).ready(function() {
		$('#patientName').keypress(function(e) {

			var k = String.fromCharCode(e.which);
			if (k.match(/[^a-zA-Z0-9*]/g)){
				console.log(k);
				e.preventDefault();}
		});
	});
</script>
</head>
<body>
	<div class="container" align="center">
		<table border="1">
			<tr>
				<td>Patient Name</td>
				<td>Email</td>
				<td>Contact NO</td>
				<td>State</td>
				<td>City</td>
				<td>Report</td>
				<td>Edit</td>
				<td>Delete</td>
			</tr>

			<c:forEach items="${appointmentList}" var="i">
				<tr>
					<td>${i.getPatientName()}</td>
					<td>${i.getEmail()}</td>
					<td>${i.getPhoneNumber()}</td>
					<td>${i.getCity().getState().getStateName()}</td>
					<td>${i.getCity().getCityName()}</td>
					<td><c:forEach items="${i.getReport()}" varStatus="loop"
							var="x">
${x.getReportName()} 
<c:if test="${!loop.last}">,</c:if>
						</c:forEach></td>
					<td><a href="editAppointment/${i.getAppointmentId()}">Edit</a></td>
					<td><a href="deleteAppointment/${i.getAppointmentId()}">Delete</a></td>
				</tr>
			</c:forEach>

		</table>
		<br> <br>
		<form action="viewAppointment" method="get">
			Patient Name:<input id="patientName" type="text" name="patientName" /><br>
			Email :<input type="text" id="email" name="email" /><br>
			ReportName :<input type="text" id="reportName" name="reportName" /><br>
			<input type="submit" value="search">
		</form>
	</div>
</body>
</html>