<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
}

* {
	box-sizing: border-box;
}

input[type=text], select {
	width: 50%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
	margin-top: 6px;
	margin-bottom: 16px;
	resize: vertical;
}

input[type=submit] {
	background-color: #4CAF50;
	color: white;
	padding: 12px 20px;
	border: none;
	border-radius: 4px;
	cursor: pointer;
}

input[type=submit]:hover {
	background-color: #45a049;
}

.container {
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 20px;
}
</style>
</head>
<body>
	<div class="container">
		<form:form modelAttribute="appointment" action="update" method="Post">
			<br>
    Patient Name:
    <form:input type="text" path="patientName" name="patientName"
				placeholder="patient name.." required="true" />
			<br>

    Email :
    <form:input type="text" path="email" name="Email"
				placeholder="email" required="true" />
			<br>
			<form:input path="appointmentId" type="hidden" name="appointmentId" />
   Phone Number : 
    <form:input type="number" name="phoneNumber" path="phoneNumber"
				placeholder="Phone Number" required="true" />
			<br>

			Report :
		
			<form:select path="report" name="report" required="true">
				<c:set value="${reportString}" var="reportString]"></c:set>
				<c:forEach items="${appointment.report}" var="y">
					<form:option selected="true" value="${y.reportId}">${y.reportName} - ${y.price}</form:option>
				</c:forEach>
				<c:forEach items="${appointment.reportList}" var="i">
					<c:choose>
						<c:when test="${fn:contains(reportString, i.reportName)}">
						</c:when>
						<c:otherwise>
							<form:option value="${i.reportId}">${i.reportName} - ${i.price}</form:option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</form:select>

			<%-- <form:select path="report" name="report" required="true">
				<c:forEach items="${appointment.reportList}" var="i">
					<c:set var="x" value="1"></c:set>
					<c:forEach items="${appointment.report}" var="y">
						<c:choose>
							<c:when test="${i.reportId eq y.reportId}">
								<form:option selected="true" value="${i.reportId}">${i.reportName}</form:option>
								<c:set var="x" value="0"></c:set>
							</c:when>
							<c:when test="${x==1}">
								<form:option value="${i.reportId}">${i.reportName}</form:option>
								<c:set var="x" value="0"></c:set>
							</c:when>
						</c:choose>
					</c:forEach>
				</c:forEach>
			</form:select> --%>

			<br>
			<br>
		State:
		
			<form:select path="state" name="state" required="true">
				<c:forEach items="${appointment.stateList}" var="i">
					<c:choose>
						<c:when test="${i.stateId eq appointment.city.state.stateId}">
							<form:option name="state" value="${i.stateId}" selected="true">${i.stateName}</form:option>
						</c:when>
						<c:otherwise>
							<form:option name="state" value="${i.stateId}">${i.stateName}</form:option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</form:select>
			<br>

			City :
			<form:select path="city" name="city" required="true">
				<c:forEach items="${appointment.cityList}" var="i">
					<c:choose>
						<c:when test="${i.cityId eq appointment.city.cityId}">
							<form:option name="city" value="${i.cityId}" selected="true">${i.cityName}</form:option>
						</c:when>
						<c:otherwise>
							<form:option name="city" value="${i.cityId}">${i.cityName}</form:option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</form:select>
			<br>
		
   Total Amount : 
    <form:input type="number" name="totalAmount" path="totalAmount"
				placeholder="Total Amount" />
			<br>
			<input type="submit" value="Submit">
		</form:form>
	</div>
</body>
</html>