<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
    crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

input[type=text], select{
  width: 50%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
</head>
<body>

	<div class="container" align="center">


		<Table border="1">
			<tr>
				<td>ReportName</td>
				<td>Price</td>
				<td>Edit</td>
				<td>Delete</td>
			</tr>
			<c:forEach items="${reportList}" var="i">
				<tr>
					<td>${i.getReportName()}</td>
					<td>${i.getPrice()}</td>
					<td><a href="editReport/${i.getReportId()}">Edit</a></td>
					<td><a href="deleteReport/${i.getReportId()}">Delete</a></td>
				</tr>
			</c:forEach>
		</Table>

		<br>
		<br>
		<form action="viewReport" method="get">
			Report Name:<input type="text" name="reportName" /><br> price :<input
				type="number" name="price"  value="0"/><br><br> <input type="submit"
				value="search">
		</form>


	</div>
</body>
</html>