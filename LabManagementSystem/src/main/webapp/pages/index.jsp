<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title></title>
<link
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
    crossorigin="anonymous">
</head>
<body>
<br>
<br>
<div align="center">
<a href="addAppointment">Schedule Appointment</a><br><br>
<a href="viewAppointment">View Appointment</a><br><br>
<a href="addReport">Add Reports</a><br><br>
<a href="viewReport">View Reports</a><br><br>
</div>
</body>
</html>