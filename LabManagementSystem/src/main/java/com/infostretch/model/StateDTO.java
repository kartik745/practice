package com.infostretch.model;

import java.util.List;

import com.infostretch.entity.City;

public class StateDTO {

		private int stateId;

		private String stateName;

		private List<City> city;

		@Override
		public String toString() {
			return "State [stateId=" + stateId + ", stateName=" + stateName  + "]";
		}

		public List<City> getCity() {
			return city;
		}

		public void setCity(List<City> city) {
			this.city = city;
		}

		public int getStateId() {
			return stateId;
		}

		public void setStateId(int stateId) {
			this.stateId = stateId;
		}

		public String getStateName() {
			return stateName;
		}

		public void setStateName(String stateName) {
			this.stateName = stateName;
		}

	
	
}
