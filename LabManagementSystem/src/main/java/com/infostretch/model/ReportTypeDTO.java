package com.infostretch.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ReportTypeDTO {
	public ReportTypeDTO() {
		super();
	}

	@Override
	public String toString() {
		return "ReportTypeDTO [reportId=" + reportId + ", reportName=" + reportName + ", price=" + price + "]";
	}

	@JsonIgnore
	private int reportId;

	private String reportName;

	private int price;
 
	
	
	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
