package com.infostretch.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.infostretch.entity.City;
import com.infostretch.entity.ReportType;
import com.infostretch.entity.State;

public class AppointmentDTO {

	@JsonIgnore
	private int appointmentId;

	private String patientName;

	private String email;

	private String phoneNumber;

	private List<ReportType> report;

	private int totalAmount;
	@JsonIgnore
	private State state;

	private City city;

	private List<ReportType> reportList;

	private List<State> stateList;

	private List<City> cityList;

	public List<ReportType> getReportList() {
		return reportList;
	}

	public void setReportList(List<ReportType> reportList) {
		this.reportList = reportList;
	}

	public List<State> getStateList() {
		return stateList;
	}

	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}

	public List<City> getCityList() {
		return cityList;
	}

	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}

	@JsonIgnore
	private String reportName;

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public int getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(int appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<ReportType> getReport() {
		return report;
	}

	public void setReport(List<ReportType> report) {
		this.report = report;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "AppointmentDTO [appointmentId=" + appointmentId + ", patientName=" + patientName + ", email=" + email
				+ ", phoneNumber=" + phoneNumber + ", report=" + report + ", totalAmount=" + totalAmount + ", state="
				+ state + ", city=" + city + ", reportName=" + reportName + "]";
	}

}
