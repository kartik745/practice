package com.infostretch.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.infostretch.entity.ReportType;
import com.infostretch.entity.State;
import com.infostretch.model.AppointmentDTO;
import com.infostretch.repository.CityRepository;
import com.infostretch.repository.ReportRepository;
import com.infostretch.repository.StateRepository;
import com.infostretch.service.AppointmentService;

@RestController
public class AppointmentController {

	@Autowired
	AppointmentService appointmentService;

	@Autowired
	StateRepository stateRepository;

	@Autowired
	ReportRepository reportRepository;

	@Autowired
	ModelMapper mapper;

	@Autowired
	CityRepository cityRepository;

	@GetMapping("/addAppointment")
	public ModelAndView displayAddAppointment() {
		List<State> stateList = stateRepository.findAll();
		List<ReportType> reportList = reportRepository.findAll();
		ModelAndView mv = new ModelAndView();
		mv.addObject("report", reportList);
		mv.addObject("state", stateList);
		mv.addObject("appointment", new AppointmentDTO());
		mv.setViewName("appointment");
		return mv;
	}

	@PostMapping("/saveAppointment")
	public ModelAndView createAppointment(@ModelAttribute AppointmentDTO appointmentDTO) {
		appointmentService.addAppointment(appointmentDTO);
		return new ModelAndView("index");
	}

	@GetMapping("/viewAppointment")
	public ModelAndView getAppointments(@ModelAttribute AppointmentDTO appointmentDTO) {
		List<AppointmentDTO> appointmentDTOList = appointmentService.searchAppointment(appointmentDTO);
		return new ModelAndView("viewAppointment", "appointmentList", appointmentDTOList);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/editAppointment/{appointmentId}")
	public ModelAndView editAppointment(@PathVariable int appointmentId) {
		AppointmentDTO appointmentDTO = appointmentService.getAppointmentById(appointmentId);
		appointmentDTO.setReportList(reportRepository.findAll());
		appointmentDTO.setCityList(cityRepository.findAll());
		appointmentDTO.setStateList(stateRepository.findAll());
		StringBuilder reportString = new StringBuilder();
		for (ReportType l : appointmentDTO.getReport()) {
			reportString.append(" ");
			reportString.append(l.getReportName());
		}

		return new ModelAndView("editAppointment", "appointment", appointmentDTO).addObject("reportString",
				reportString);
	}
	
	@PostMapping("/editAppointment/update")
	public ModelAndView updateAppointmentById(@RequestParam int appointmentId,
			@ModelAttribute AppointmentDTO appointmentDTO) {
		appointmentService.updateAppointmentById(appointmentId, appointmentDTO);
		return new ModelAndView("redirect:/");
	}

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/deleteAppointment/{appointmentId}")
	public ModelAndView deleteAppointmentById(@PathVariable int appointmentId) {

		appointmentService.deleteAppointmentById(appointmentId);
		return new ModelAndView("redirect:/");
	}

}
