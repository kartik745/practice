package com.infostretch.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.infostretch.entity.User;
import com.infostretch.model.Token;
import com.infostretch.repository.UserRepository;
import com.infostretch.security.MyUserDetailsService;
import com.infostretch.security.JWTToken.JwtProvider;

@Controller
public class LoginController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	UserRepository userRepository;

	@GetMapping("/signin")
	public ResponseEntity<Token> signin() {

		String loggedInUser = MyUserDetailsService.currentUserDetails().getUsername();
		String jwt = jwtProvider.generateJwtToken(loggedInUser);
		System.out.println("-----------jwt----" + jwt);
		Optional<User> user = userRepository.findByUserName(loggedInUser);
		if (user.isPresent()) {
			Token token = new Token();
			token.setJwtToken(jwt);
			return new ResponseEntity<>(token, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@GetMapping("/loginpage")
	public ModelAndView login() {
		return new ModelAndView("login");
	}

}
