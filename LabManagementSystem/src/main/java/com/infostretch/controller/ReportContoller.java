package com.infostretch.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.infostretch.model.ReportTypeDTO;
import com.infostretch.service.ReportService;

@RestController
public class ReportContoller {

	@Autowired
	ReportService reportService;

	@GetMapping("/calculateAmount/{id}")
	public Integer demo(@PathVariable("id") String list) {

		return reportService.calculateAmount(list);
	}

	@GetMapping("/addReport")
	public ModelAndView addReport() {
		return new ModelAndView("addReportType");
	}

	@PostMapping("/saveReport")
	public ModelAndView addReport(@ModelAttribute ReportTypeDTO reportTypeDTO) {
		reportService.addReport(reportTypeDTO);
		return new ModelAndView("index");
	}

	@GetMapping("/viewReport")
	public ModelAndView viewReport(@ModelAttribute ReportTypeDTO reportTypeDTO) {
		List<ReportTypeDTO> reportTypeDTOList = reportService.searchReport(reportTypeDTO);
		return new ModelAndView("viewReportType", "reportList", reportTypeDTOList);
	}

	@GetMapping("/editReport/{reportId}")
	public ModelAndView editReport(@PathVariable int reportId, @ModelAttribute ReportTypeDTO reportTypeDTO) {
		ReportTypeDTO reportDTO = reportService.viewReportById(reportId);
		return new ModelAndView("editReport", "report", reportDTO);
	}

	@PostMapping("editReport/updateReport")
	public ModelAndView updateReportById(@RequestParam int reportId, @ModelAttribute ReportTypeDTO reportTypeDTO) {
		reportService.updateReport(reportId, reportTypeDTO);
		return new ModelAndView("redirect:/");
	}

	@GetMapping("/deleteReport/{reportId}")
	public ModelAndView deleteReport(@PathVariable int reportId) {
		reportService.deleteReportById(reportId);
		return new ModelAndView("redirect:/");
	}

}
