package com.infostretch.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.infostretch.entity.City;
import com.infostretch.service.StateServiceImpl;

@Controller
public class StateController {

	@Autowired
	StateServiceImpl stateService;

	@GetMapping("getCity")
	public ModelAndView searchByStateName(@RequestParam("stateId") int stateId) {
		List<City> cityList = stateService.getcity(stateId);
		return new ModelAndView("jsonLoadCityData", "city", cityList);

	}

}
