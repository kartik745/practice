package com.infostretch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infostretch.entity.City;

public interface CityRepository extends JpaRepository<City, Integer> {

}
