package com.infostretch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.infostretch.entity.ReportType;

@Repository
public interface ReportRepository extends JpaRepository<ReportType, Integer> {

	@Query("Select c from ReportType c where c.reportName like :reportName%")
	public List<ReportType> findByReportName(String reportName);
	
	public List<ReportType> findByPrice(int price);
	
}
