package com.infostretch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infostretch.entity.State;

public interface StateRepository extends JpaRepository<State,Integer> {
public State findByStateName(String statenName);
}
