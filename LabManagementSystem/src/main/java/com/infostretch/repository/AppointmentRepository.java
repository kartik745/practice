package com.infostretch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.infostretch.entity.Appointment;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {

	@Query("Select c from Appointment c inner join c.report r where c.patientName like :patientName% AND c.email like :email% AND r.reportName like :reportName% " )
	public List<Appointment> findByPatientNameAndEmailAndReportReportName(String patientName,String email,String reportName);
	
	@Query("Select c from Appointment c where c.patientName like :patientName% AND c.email like :email%")
	public List<Appointment> findByPatientNameAndEmail(String patientName,String email);
	
	@Query("Select c from Appointment c inner join c.report r where c.patientName like :patientName% AND r.reportName like :reportName%")
	public List<Appointment> findByPatientNameAndReportReportName(String patientName,String reportName);

	@Query("Select c from Appointment c  inner join c.report r where c.email like :email% AND r.reportName like :reportName%")
	public List<Appointment> findByEmailAndReportReportName(String email,String reportName);
	
	@Query("Select c from Appointment c where c.email like :email%")
	public List<Appointment> findByEmail(String email);
	
	@Query("Select c from Appointment c where c.patientName like :patientName%")
	public List<Appointment> findByPatientName(String patientName);
	
	@Query("Select c from Appointment c inner join c.report r where r.reportName like :reportName%")
	public List<Appointment> findByReportReportName(String reportName);
}
