package com.infostretch.utility;

import java.util.Comparator;

import com.infostretch.entity.Appointment;

public class SortByNameForAppointment implements Comparator<Appointment> {

	@Override
	public int compare(Appointment o1, Appointment o2) {
		if (o1.getPatientName().equalsIgnoreCase(o2.getPatientName())) {
			if(o1.getCity().getCityName().equalsIgnoreCase(o2.getCity().getCityName()))
			{
				if (o1.getAppointmentId() > o2.getAppointmentId())
					return 1;
				else if (o1.getAppointmentId() < o2.getAppointmentId())
					return -1;
			}else {
				return o1.getCity().getCityName().compareTo(o2.getCity().getCityName());
			}
		} else {
			return o1.getPatientName().compareTo(o2.getPatientName());
		}
		return 0;
	}
}
