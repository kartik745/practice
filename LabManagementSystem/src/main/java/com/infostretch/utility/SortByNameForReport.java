package com.infostretch.utility;

import java.util.Comparator;

import com.infostretch.entity.ReportType;

public class SortByNameForReport implements Comparator<ReportType> {

	@Override
	public int compare(ReportType o1, ReportType o2) {
		if(o1.getReportName().equalsIgnoreCase(o2.getReportName())) {
			if(o1.getPrice()>o2.getPrice())
				return 1;
			else if(o1.getPrice()<o2.getPrice())
				return -1;
		}else {
			return o1.getReportName().compareTo(o2.getReportName());
		}
		return 0;
	}

}
