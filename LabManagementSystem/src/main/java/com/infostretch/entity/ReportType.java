package com.infostretch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "reportType")
public class ReportType {

	@Override
	public String toString() {
		return "ReportType [reportId=" + reportId + ", reportName=" + reportName + ", price=" + price + "]";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int reportId;

	@NotNull
	@Column(unique = true)
	private String reportName;

	@NotNull
	private int price;

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
