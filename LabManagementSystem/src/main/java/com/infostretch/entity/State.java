package com.infostretch.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class State {
	@Id
	private int stateId;

	private String stateName;

	@OneToMany(mappedBy = "state")
	@JsonIgnore
	private List<City> city;

	@Override
	public String toString() {
		return "State [stateId=" + stateId + ", stateName=" + stateName  + "]";
	}

	public List<City> getCity() {
		return city;
	}

	public void setCity(List<City> city) {
		this.city = city;
	}

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}
