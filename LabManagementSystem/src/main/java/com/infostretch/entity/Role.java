package com.infostretch.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {

	@Override
	public String toString() {
		return "Role [roleId=" + roleId + ", role=" + roleType + "]";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int roleId;

	private String roleType;

	public Role() {
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

}
