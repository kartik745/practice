package com.infostretch.security;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.infostretch.entity.Role;
import com.infostretch.entity.User;
import com.infostretch.repository.UserRepository;

@Component
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username)  {
		Optional<User> user = userRepository.findByUserName(username);
		if (user.isPresent()) {
			Set<GrantedAuthority> authorities = new HashSet<>();
			for (Role role : user.get().getRoles()) {
				authorities.add(new SimpleGrantedAuthority(role.getRoleType()));
			}
			return new org.springframework.security.core.userdetails.User(user.get().getUserName(),
					passwordEncoder.encode(user.get().getPassword()), authorities);
		} else {
			throw new UsernameNotFoundException("user not present with this userName : " + username);
		}
	}

	public static UserDetails currentUserDetails() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			Object principal = authentication.getPrincipal();
			return principal instanceof UserDetails ? (UserDetails) principal : null;
		}
		return null;
	}
}
