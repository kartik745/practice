package com.infostretch.service;

import java.util.List;

import com.infostretch.entity.Appointment;
import com.infostretch.model.AppointmentDTO;

public interface AppointmentService {
	public Appointment addAppointment(AppointmentDTO appointmentDTO);
	public List<AppointmentDTO> getAppointments();
	public AppointmentDTO getAppointmentById(int appointmentId);
	public AppointmentDTO updateAppointmentById(int appointmentId, AppointmentDTO appointmentDTO);
	public void deleteAppointmentById(int appointmentId);
	public List<AppointmentDTO> searchAppointment(AppointmentDTO appointmentDTO);
}
