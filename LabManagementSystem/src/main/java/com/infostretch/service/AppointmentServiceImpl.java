package com.infostretch.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infostretch.dao.AppointmentDao;
import com.infostretch.entity.Appointment;
import com.infostretch.model.AppointmentDTO;
import com.infostretch.utility.SortByNameForAppointment;

@Service
public class AppointmentServiceImpl implements AppointmentService {

	@Autowired
	AppointmentDao appointmentDao;
	@Autowired
	ModelMapper mapper;

	public Appointment addAppointment(AppointmentDTO appointmentDTO) {
		Appointment appointment = mapper.map(appointmentDTO, Appointment.class);
		return appointmentDao.addAppointment(appointment);
	}

	public List<AppointmentDTO> getAppointments() {
		List<Appointment> appointmentList = appointmentDao.getAppointments();
		// sorting
		Collections.sort(appointmentList, new SortByNameForAppointment());
		return appointmentList.stream().map(appointment -> mapper.map(appointment, AppointmentDTO.class))
				.collect(Collectors.toList());
	}

	public AppointmentDTO getAppointmentById(int appointmentId) {
		Appointment appointment = appointmentDao.getAppointmentById(appointmentId);
		return mapper.map(appointment, AppointmentDTO.class);
	}

	public AppointmentDTO updateAppointmentById(int appointmentId, AppointmentDTO appointmentDTO) {
		Appointment appointment = mapper.map(appointmentDTO, Appointment.class);
		appointmentDao.updateAppointmentById(appointmentId, appointment);
		return appointmentDTO;
	}

	public void deleteAppointmentById(int appointmentId) {
		appointmentDao.deleteAppointmentById(appointmentId);

	}

	public List<AppointmentDTO> searchAppointment(AppointmentDTO appointmentDTO) {
		String patientName = appointmentDTO.getPatientName();
		String email = appointmentDTO.getEmail();
		String reportName = appointmentDTO.getReportName();
		List<Appointment> appointmentList;
		if (patientName != null && !patientName.isEmpty() && email != null && !email.isEmpty() && reportName != null
				&& !reportName.isEmpty()) {
			appointmentList= appointmentDao.searchByPatientNameAndEmailAndReportName(patientName, email, reportName);
			Collections.sort(appointmentList, new SortByNameForAppointment());
			return appointmentList.stream().map(a -> mapper.map(a, AppointmentDTO.class)).collect(Collectors.toList());
		} else if (patientName != null && !patientName.isEmpty() && email != null && !email.isEmpty()) {
			appointmentList=appointmentDao.searchByPatientNameAndEmail(patientName, email);
			Collections.sort(appointmentList, new SortByNameForAppointment());
			return appointmentList.stream().map(a -> mapper.map(a, AppointmentDTO.class)).collect(Collectors.toList());
		} else if (email != null && !email.isEmpty() && reportName != null && !reportName.isEmpty()) {
			appointmentList=appointmentDao.searchByEmailAndReportName(email, reportName);
			Collections.sort(appointmentList, new SortByNameForAppointment());
			return appointmentList.stream().map(a -> mapper.map(a, AppointmentDTO.class)).collect(Collectors.toList());
		} else if (patientName != null && !patientName.isEmpty() && reportName != null && !reportName.isEmpty()) {
			appointmentList= appointmentDao.searchByPatientNameAndReportName(patientName, reportName);
			Collections.sort(appointmentList, new SortByNameForAppointment());
			return appointmentList.stream().map(a -> mapper.map(a, AppointmentDTO.class)).collect(Collectors.toList());
		} else if (patientName != null && !patientName.isEmpty()) {
			appointmentList = appointmentDao.searchByPatientName(patientName);
			Collections.sort(appointmentList, new SortByNameForAppointment());
			return appointmentList.stream().map(a -> mapper.map(a, AppointmentDTO.class)).collect(Collectors.toList());
		} else if (email != null && !email.isEmpty()) {
			appointmentList=appointmentDao.searchByEmail(email);
			Collections.sort(appointmentList, new SortByNameForAppointment());
			return appointmentList.stream().map(a -> mapper.map(a, AppointmentDTO.class)).collect(Collectors.toList());
		} else if (reportName != null && !reportName.isEmpty()) {
			appointmentList = appointmentDao.searchByReportName(reportName);
			Collections.sort(appointmentList, new SortByNameForAppointment());
			return appointmentList.stream().map(a -> mapper.map(a, AppointmentDTO.class)).collect(Collectors.toList());
		} else {
			appointmentList = appointmentDao.getAppointments();
			Collections.sort(appointmentList, new SortByNameForAppointment());
			return appointmentList.stream().map(a -> mapper.map(a, AppointmentDTO.class)).collect(Collectors.toList());
		}
	}
}
