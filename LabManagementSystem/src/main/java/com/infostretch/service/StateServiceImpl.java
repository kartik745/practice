package com.infostretch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infostretch.dao.StateDao;
import com.infostretch.entity.City;
import com.infostretch.entity.State;

@Service
public class StateServiceImpl {

	@Autowired
	StateDao stateDao;
	
	public List<City> getcity(int stateId) {
		State state=stateDao.getCityList(stateId);
		return state.getCity();
	}

}
