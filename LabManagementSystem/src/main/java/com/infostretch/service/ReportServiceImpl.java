package com.infostretch.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infostretch.dao.ReportDao;
import com.infostretch.entity.ReportType;
import com.infostretch.model.ReportTypeDTO;
import com.infostretch.utility.SortByNameForReport;

@Service
public class ReportServiceImpl implements ReportService{

	@Autowired
	ReportDao reportDao;
	@Autowired
	ModelMapper mapper;

	public ReportType addReport(ReportTypeDTO reportTypeDTO) {
		ReportType reportType = mapper.map(reportTypeDTO, ReportType.class);
		return reportDao.addReport(reportType);

	}
	public ReportTypeDTO viewReportById(int reportId) {
		ReportType report = reportDao.viewReportById(reportId);
		List<ReportType> reportList=new ArrayList<>();
		reportList.add(report);
		Collections.sort(reportList,new SortByNameForReport());

		return mapper.map(report,ReportTypeDTO.class);
	}

	public List<ReportTypeDTO> viewReport() {
		List<ReportType> reportTypeList = reportDao.viewReport();
		Collections.sort(reportTypeList,new SortByNameForReport());

		return reportTypeList.stream().map(report -> mapper.map(report, ReportTypeDTO.class))
				.collect(Collectors.toList());
	}

	public ReportTypeDTO updateReport(int reportId, ReportTypeDTO reportTypeDTO) {
		ReportType reportType = mapper.map(reportTypeDTO, ReportType.class);
		reportDao.updateReport(reportId, reportType);
		return reportTypeDTO;
	}

	public void deleteReportById(int reportId) {
		reportDao.deleteReportById(reportId);
	}

	public List<ReportTypeDTO> searchReport(ReportTypeDTO reportTypeDTO) {
		String reportType = reportTypeDTO.getReportName();
		int price = reportTypeDTO.getPrice();
		
		List<ReportType> reportList;
		if (reportType != null && !"".equals(reportType)) {
			 reportList=reportDao.searchByName(reportType);
			
			return reportList.stream().map(report->mapper.map(report, ReportTypeDTO.class)).collect(Collectors.toList());
		} else if(price >0 ){
			 reportList=reportDao.searchByPrice(price);
			return reportList.stream().map(report->mapper.map(report, ReportTypeDTO.class)).collect(Collectors.toList());
		}else {
			reportList=reportDao.viewReport();
			return reportList.stream().map(report->mapper.map(report, ReportTypeDTO.class)).collect(Collectors.toList());
			
		}

	}
	public Integer calculateAmount(String list) {
		List<Integer> listOfId=new ArrayList<>();
		String a1[]=list.split(",");
		for(String i: a1 ) {
			int y=Integer.parseInt(i);
			listOfId.add(y);
		}
	return reportDao.calculateAmount(listOfId);
		
	}

}
