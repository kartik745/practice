package com.infostretch.service;

import java.util.List;

import com.infostretch.entity.ReportType;
import com.infostretch.model.ReportTypeDTO;

public interface ReportService  {

	
	public ReportType addReport(ReportTypeDTO reportTypeDTO);
	public ReportTypeDTO viewReportById(int reportId);
	public List<ReportTypeDTO> viewReport();
	public ReportTypeDTO updateReport(int reportId, ReportTypeDTO reportTypeDTO);
	public void deleteReportById(int reportId);
	public List<ReportTypeDTO> searchReport(ReportTypeDTO reportTypeDTO);
	public Integer calculateAmount(String arrayOfId);
	
}
