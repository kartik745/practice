package com.infostretch.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.infostretch.CustomException.AppointmentNotFoundException;
import com.infostretch.entity.Appointment;
import com.infostretch.repository.AppointmentRepository;

@Repository
public class AppointmentDao {

	@Autowired
	AppointmentRepository appointmentRepository;

	public List<Appointment> nullCheck(List<Appointment> appointment, String msg) {
		if (!appointment.isEmpty())
			return appointment;
		else
			throw new AppointmentNotFoundException(msg);
	}

	public Appointment addAppointment(Appointment appointment) {
		return appointmentRepository.save(appointment);
	}

	public List<Appointment> getAppointments() {

		return appointmentRepository.findAll();
	}

	public Appointment getAppointmentById(int appointmentId) {
		Optional<Appointment> appointment = appointmentRepository.findById(appointmentId);
		if (appointment.isPresent())
			return appointment.get();
		else
			throw new AppointmentNotFoundException("Appointment Id not Found");

	}

	public Appointment updateAppointmentById(int appointmentId, Appointment appointment) {
		if (appointmentRepository.findById(appointmentId).isPresent())
			return appointmentRepository.save(appointment);
		else
			throw new AppointmentNotFoundException("Appointment Id not Found");

	}

	public void deleteAppointmentById(int appointmentId) {
		Optional<Appointment> appointment = appointmentRepository.findById(appointmentId);
		if (appointment.isPresent())
			appointmentRepository.deleteById(appointmentId);
		else
			throw new AppointmentNotFoundException("Appointment Id not Found");
	}

	public List<Appointment> searchByPatientNameAndEmailAndReportName(String patientName, String email, String reportName) {
		List<Appointment> appointment = appointmentRepository.findByPatientNameAndEmailAndReportReportName(patientName, email,
				reportName);
		String msg = "PatientName or email or ReportName not found";
		return nullCheck(appointment, msg);

	}

	public List<Appointment> searchByPatientNameAndEmail(String patientName, String email) {
		List<Appointment> appointment = appointmentRepository.findByPatientNameAndEmail(patientName, email);
		String msg = "Email or PatientName not found";
		return nullCheck(appointment, msg);
	}
	

	public List<Appointment> searchByEmailAndReportName(String email, String reportName) {
		List<Appointment> appointment = appointmentRepository.findByEmailAndReportReportName(email, reportName);
		String msg = "Email or Report not found";
		return nullCheck(appointment, msg);
	}

	public List<Appointment> searchByPatientNameAndReportName(String patientName, String reportName) {
		List<Appointment> appointment = appointmentRepository.findByPatientNameAndReportReportName(patientName, reportName);
		String msg = "patientName or Report not found";
		return nullCheck(appointment, msg);
	}

	public List<Appointment> searchByPatientName(String patientName) {
		List<Appointment> appointment = appointmentRepository.findByPatientName(patientName);
		String msg = "PatientName not found";
		return nullCheck(appointment, msg);
	}

	public List<Appointment> searchByEmail(String email) {
		List<Appointment> appointment = appointmentRepository.findByEmail(email);
		String msg = "email not found";
		return nullCheck(appointment, msg);
	}
	

	public List<Appointment> searchByReportName(String reportName) {
		List<Appointment> appointment = appointmentRepository.findByReportReportName(reportName);
		String msg = "report not found";
		return nullCheck(appointment, msg);
	}
	}


