package com.infostretch.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.infostretch.entity.State;
import com.infostretch.repository.StateRepository;

@Repository
public class StateDao {
	@Autowired
	StateRepository stateRepository;

	public State getCityList(int stateId) {
Optional<State> state=stateRepository.findById(stateId);
if(state.isPresent())
	return state.get();
	else
		return null;
}
}
