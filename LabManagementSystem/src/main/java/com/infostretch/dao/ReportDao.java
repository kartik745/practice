package com.infostretch.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.infostretch.CustomException.ReportNotFoundException;
import com.infostretch.entity.ReportType;
import com.infostretch.repository.ReportRepository;

@Component
public class ReportDao {
	@Autowired
	ReportRepository reportRepository;

	public ReportType addReport(ReportType report) {

		return reportRepository.save(report);
	}

	public List<ReportType> viewReport() {
		return reportRepository.findAll();
	}

	public ReportType updateReport(int reportId, ReportType reportType) {
		Optional<ReportType> report = reportRepository.findById(reportId);
		if (report.isPresent()) {
			return reportRepository.save(reportType);
		} else {
			throw new ReportNotFoundException("Report Id not found");
		}
	}

	public void deleteReportById(int reportId) {
		if (reportRepository.findById(reportId).isPresent()) {
			reportRepository.deleteById(reportId);
		} else {
			throw new ReportNotFoundException("Report Id not found");
		}
	}

	public List<ReportType> searchByName(String reportType) {
		List<ReportType> report = reportRepository.findByReportName(reportType);
		if (!report.isEmpty()) {
			return report;
		} else {
			throw new ReportNotFoundException("report  not found");
		}
	}

	public List<ReportType> searchByPrice(int price) {
		List<ReportType> report = reportRepository.findByPrice(price);
		if (!report.isEmpty()) {
			return report;
		} else {
			throw new ReportNotFoundException("report not found");
		}
	}

	public ReportType viewReportById(int reportId) {
		Optional<ReportType> report=reportRepository.findById(reportId);
		if (report.isPresent()) 
			return report.get();
		else
			throw new ReportNotFoundException("report  not found");
	}

	public Integer calculateAmount(List<Integer> listOfId) {
int totalAmount=0;
for(int i:listOfId) {
	totalAmount=totalAmount+reportRepository.findById(i).get().getPrice();
}
System.out.println(totalAmount+"-amount------");
return totalAmount;
	}

}
