package com.infostretch.CustomException;

public class AppointmentNotFoundException extends RuntimeException {


	private static final long serialVersionUID = 8037411399687632158L;

	public AppointmentNotFoundException(String message){
		super(message);
	}
}
