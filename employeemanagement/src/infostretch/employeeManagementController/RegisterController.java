package infostretch.employeeManagementController;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import infostretch.employeeManagementModel.Employee;
import infostretch.employeeManagementModel.JDBCConnection;

@WebServlet("/RegisterController")
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RegisterController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/Register.jsp");
		rs.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Employee employee;
		String name = request.getParameter("name");
		String contactNo = request.getParameter("contactNo");
		String password=request.getParameter("password");
		String encryptedPassword=JDBCConnection.encryptPassword(password);
		employee = new Employee();
		employee.setEmployeeContactNo(contactNo);
		employee.setEmployeeName(name);
		employee.setPassword(encryptedPassword);
		try {
			JDBCConnection.addEmployee(employee);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/login.jsp");
		rs.forward(request, response);
	}

}
