package infostretch.employeeManagementController;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import infostretch.employeeManagementModel.Employee;
import infostretch.employeeManagementModel.JDBCConnection;

@WebServlet("/ViewUser")
public class ViewUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ViewUserController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getSession().getAttribute("userName") != null) {
			int id = Integer.parseInt(request.getParameter("id"));
			Employee employee = null;
			try {
				employee = JDBCConnection.getEmployee(id);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			request.setAttribute("employee", employee);
			RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/viewUser.jsp");
			rs.forward(request, response);
		} else {
			System.out.println("session invalidate");
			RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/login.jsp");
			rs.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
