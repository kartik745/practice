package infostretch.employeeManagementController;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import infostretch.employeeManagementModel.Employee;
import infostretch.employeeManagementModel.JDBCConnection;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/login.jsp");
		rs.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userName1 = request.getParameter("userName");
		String password = request.getParameter("password");
		String encryptedPassword=JDBCConnection.encryptPassword(password);
		if (userName1.equals("admin") && password.equals("admin")) {
			//HttpSession session=request.getSession();
			//session.setAttribute("userName", userName1);
			RequestDispatcher rs = request.getRequestDispatcher("AdminController");
			rs.forward(request, response);
		} else {
			Employee employee = null;
			try {
				employee = JDBCConnection.verifyEmployee(userName1, encryptedPassword);
				if (employee != null) {
					HttpSession session=request.getSession(true);
					session.setAttribute("userName", userName1);
					request.setAttribute("employee", employee);
					RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/welcome.jsp");
					rs.forward(request, response);
					//response.sendRedirect("WEB-INF/views/welcome.jsp");
				} else {
					PrintWriter out = response.getWriter();
					out.print("enter valid UserName-Password");
					RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/login.jsp");
					rs.forward(request, response);
				}
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
