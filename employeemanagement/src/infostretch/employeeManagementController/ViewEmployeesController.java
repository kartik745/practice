package infostretch.employeeManagementController;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import infostretch.employeeManagementModel.Employee;
import infostretch.employeeManagementModel.JDBCConnection;
@WebServlet("/viewemployee")
public class ViewEmployeesController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Employee> employeeDetails = null;
		try {
			employeeDetails = JDBCConnection.viewEmployee();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} 
		request.setAttribute("employeeDetails", employeeDetails);
		RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/viewEmployees.jsp");
		rs.forward(request, response);
	}

}
