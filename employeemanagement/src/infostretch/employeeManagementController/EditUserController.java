package infostretch.employeeManagementController;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import infostretch.employeeManagementModel.Employee;
import infostretch.employeeManagementModel.JDBCConnection;

@WebServlet("/EditUser")
public class EditUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public EditUserController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Employee employee = null;
		try {
			 employee= JDBCConnection.getEmployee(id);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("employee", employee);
		RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/editUser.jsp");
		rs.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		String name=request.getParameter("name");
		String contactNo = request.getParameter("contactNo");
		String password=request.getParameter("password");
		String encryptedPassword=JDBCConnection.encryptPassword(password);
		Employee employee = null;
		try {
			JDBCConnection.editUser(id, name, contactNo,encryptedPassword);
			employee=JDBCConnection.getEmployee(id);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("employee", employee);
		RequestDispatcher rs = request.getRequestDispatcher("WEB-INF/views/welcome.jsp");
		rs.forward(request, response);
	}

}
