package infostretch.employeeManagementModel;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class JDBCConnection  {
	static Properties prop = new Properties();

	static Connection con;
	static Statement statement;
	static int rowsUpdated;

	public static Statement getDBConnection() throws ClassNotFoundException, SQLException, IOException {
		FileInputStream fp = new FileInputStream("D:\\employee\\employeemanagement\\resources\\config.properties");
		prop.load(fp);
		Class.forName(prop.getProperty("Class_Name"));
		con = DriverManager.getConnection(prop.getProperty("DB_url"), prop.getProperty("DB_username"),
				prop.getProperty("DB_password"));
		statement = con.createStatement();
		return statement;
	}

	public static void connectionClose() throws SQLException {
		con.close();
		statement.close();
	}

	public static void addEmployee(Employee employee) throws SQLException, ClassNotFoundException, IOException {

		String name = employee.getEmployeeName();
		String contactNo = employee.getEmployeeContactNo();
		String password = employee.getPassword();
		rowsUpdated = getDBConnection().executeUpdate("insert into Employee(name,contactNo,password) values('" + name
				+ "','" + contactNo + "','" + password + "')");
		connectionClose();
	}

	public static Employee getEmployee(int id) throws ClassNotFoundException, SQLException, IOException {
		ResultSet resultSet = getDBConnection().executeQuery("select * from Employee where id='" + id + "'");
		resultSet.next();
		Employee employee = new Employee();
		employee.setEmployeeId(resultSet.getInt("id"));
		employee.setEmployeeName(resultSet.getString("name"));
		employee.setEmployeeContactNo(resultSet.getString("contactNo"));
		employee.setPassword(resultSet.getString("password"));
		connectionClose();
		return employee;
	}

	public static List<Employee> viewEmployee() throws ClassNotFoundException, SQLException, IOException {

		ResultSet resultSet = getDBConnection().executeQuery("select * from Employee");
		List<Employee> list = new ArrayList<>();
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String employeeName = resultSet.getString("name");
			String employeeContactNo = resultSet.getString("contactNo");
			Employee e = new Employee();
			e.setEmployeeContactNo(employeeContactNo);
			e.setEmployeeId(id);
			e.setEmployeeName(employeeName);
			list.add(e);
		}
		connectionClose();
		return list;
	}

	public static void deleteEmployee(int id) throws ClassNotFoundException, SQLException, IOException {
		rowsUpdated = getDBConnection().executeUpdate("delete from Employee where id='" + id + "'");
		connectionClose();
	}

	public static void editEmployee(int id, String name, String contactNo)
			throws ClassNotFoundException, SQLException, IOException {
		rowsUpdated = getDBConnection().executeUpdate(
				"update Employee set name='" + name + "',contactNo='" + contactNo + "' where id='" + id + "'");
		connectionClose();

	}
	public static void editUser(int id, String name, String contactNo,String password)
			throws ClassNotFoundException, SQLException, IOException {
		rowsUpdated = getDBConnection().executeUpdate(
				"update Employee set name='" + name + "',contactNo='" + contactNo + "',password='" + password + "' where id='" + id + "'");
		connectionClose();

	}

	public static Employee verifyEmployee(String userName, String password)
			throws ClassNotFoundException, SQLException, IOException {
		ResultSet resultSet = null;
		resultSet = getDBConnection()
				.executeQuery("select * from Employee where name='" + userName + "' and password='" + password + "'");
		if (resultSet.next()) {
			Employee employee = new Employee();
			employee.setEmployeeId(resultSet.getInt("id"));
			employee.setEmployeeName(resultSet.getString("name"));
			employee.setEmployeeContactNo(resultSet.getString("contactNo"));
			employee.setPassword(resultSet.getString("password"));
			connectionClose();
			return employee;
		} else
			return null;
	}
	public static String encryptPassword(String password) {
		MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
            byte[] passBytes = password.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<digested.length;i++){
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
		return sb.toString();
	}
	
	

}
