<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ page language="java" import="java.util.*,infostretch.employeeManagementModel.Employee"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
    function deleteRecord(empId)
    {
          if (confirm("Are you sure want to delete This record: ")) 
          {
                  $.ajax({
                      url: 'deleteemployee?id='+empId,
                      type: 'post',
                      success: function(response){
                          alert("record deleted successfully..");
                          location.reload();
                      },
                      error: function(response){
                          console.log("error response"+response);
                      }
                  });
          }
    }
/*     function editRecord(empId)
    {
          if (confirm("Are you sure want to Edit This record: ")) 
          {
                  $.ajax({
                      url: 'editemployee?id='+empId,
                      type: 'get',
                      success: function(response){
                          alert("record Edited successfully..");
                      },
                      error: function(response){
                          console.log("error response"+response);
                      }
                  });
          }
    } */
</script>
</head>
<body>
	<h3>EMPLOYEE DETAIL FROM:</h3>
	<table border="1">
		<tr>
			<th>employeeId</th>
			<th>employeeName</th>
			<th>employeeContactNo</th>
			<th>DELETE</th>
			<th>EDIT</th>
		</tr>
		<%
			List<Employee> employeeList = (List) request.getAttribute("employeeDetails");
			request.setAttribute("eList", employeeList);
		%>
		<c:forEach items="${eList}" var="employee">
			<tr>
				<td>${employee.employeeId}</td>
				<td>${employee.getEmployeeName()}</td>
				<td>${employee.getEmployeeContactNo()}</td>
				<td>
					<button onclick="deleteRecord(${employee.employeeId})">Delete</button>
				</td>
				<td><form action="editemployee" method="get"><input type="hidden"  name="id" value="${employee.employeeId}"><input type="submit" value="Edit"></form></td>
			</tr>
		</c:forEach>
	</table>
</body>



</html>