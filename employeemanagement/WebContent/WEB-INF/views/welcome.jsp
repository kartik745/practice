<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="javax.servlet.http.HttpSession"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		session = request.getSession();
		System.out.println("out of if :" + session.getAttribute("userName"));
		if (session.getAttribute("userName") == null) {
			System.out.println("no session");
			response.sendRedirect("WEB-INF/views/login.jsp");
		} else {
			System.out.print("with session " + session.getAttribute("userName"));
		}
	%>
	welcome ${employee.getEmployeeName()}

	<a href="ViewUser?id=${employee.getEmployeeId()}">view your details</a>
	<br>
	<a href="EditUser?id=${employee.getEmployeeId()}">edit your details</a>
	<a href="LogoutController">Logout</a>

</body>
</html>