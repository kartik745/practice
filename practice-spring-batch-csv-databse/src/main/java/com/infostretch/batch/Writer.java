package com.infostretch.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.infostretch.repository.UserRepository;

import co.infostretch.entity.User;

@Component
public class Writer implements ItemWriter<User>{
	@Autowired
	private UserRepository repo;

	@Transactional
	@Override
	public void write(List<? extends User> user) throws Exception {
		repo.saveAll(user);
	}
}
