package com.infostretch.batch;

import java.util.Optional;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.infostretch.repository.UserRepository;
import co.infostretch.entity.User;

@Component
public class Processor implements ItemProcessor<User, User> {
	@Autowired
	private UserRepository userRepo;

	@Override
	public User process(User user) throws Exception {
		Optional<User> userFromDb = userRepo.findById(user.getId());
		if (userFromDb.isPresent()) {
			return user;
		}
		return user;
	}

}
