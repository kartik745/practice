package com.infostretch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.infostretch.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> { 

}
