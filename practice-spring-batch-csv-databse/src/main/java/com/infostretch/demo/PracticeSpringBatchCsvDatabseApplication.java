package com.infostretch.demo;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableBatchProcessing
public class PracticeSpringBatchCsvDatabseApplication implements CommandLineRunner{
	
	public static void main(String[] args) {
		SpringApplication.run(PracticeSpringBatchCsvDatabseApplication.class, args);
	}
	 
	@Override
	public void run(String... args) throws Exception {
	}

}
