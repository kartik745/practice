package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;
import com.example.demo.model.Token;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.JwtTokenRepository;
import com.example.demo.security.JWTProvider;

@RestController
public class HomeController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JWTProvider jwtProvider;

	@Autowired
	JwtTokenRepository tokenRepo;

	@Autowired
	EmployeeRepository employeeRepository;

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String admin() {
		return "welcome admin";
	}

	@GetMapping("/user")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public String user() {
		return "welcome user";
	}

	@GetMapping("/userOnly")
	@PreAuthorize("hasRole('USER')")
	public String userOnly() {
		return "only user";
	}

	@PostMapping("/signin")
	public ResponseEntity<Token> signin(@RequestParam("email") String email,
			@RequestParam("password") String password) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(email, password));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateToken(authentication);
		Optional<Employee> employee = employeeRepository.findByEmail(email);
		if (employee.isPresent()) {
			Token token = new Token();
			token.setJwtToken(jwt);
			tokenRepo.save(token);
			return new ResponseEntity<>(token, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
