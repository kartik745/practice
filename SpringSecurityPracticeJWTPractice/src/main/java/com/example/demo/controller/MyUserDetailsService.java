package com.example.demo.controller;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.example.demo.model.Employee;
import com.example.demo.model.Role;
import com.example.demo.repository.EmployeeRepository;
@Component
public class MyUserDetailsService implements UserDetailsService  {

	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public UserDetails loadUserByUsername(String email) {
		Optional<Employee> user = employeeRepository.findByEmail(email);
		if (user.isPresent()) {
			//return new User(user.get().getEmail(), user.get().getPassword());
			 Set<GrantedAuthority> authorities = new HashSet<>();
		        for (Role role : user.get().getRoles()) {
		            authorities.add(new SimpleGrantedAuthority(role.getRole()));
		        }
		        return new User(user.get().getEmail(), user.get().getPassword(), authorities);
		} else {
			throw new UsernameNotFoundException("username not found---------->" + email);
		}
	}
}
