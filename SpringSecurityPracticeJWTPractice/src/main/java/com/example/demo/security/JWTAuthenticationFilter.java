package com.example.demo.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import com.example.demo.controller.MyUserDetailsService;
import com.example.demo.repository.JwtTokenRepository;

public class JWTAuthenticationFilter extends OncePerRequestFilter {
	private static final Logger logger1 = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

	@Autowired
	JwtTokenRepository jwtTokenRepository;
	
	@Autowired
	JWTProvider jwtProvider;
	
	@Autowired
	private MyUserDetailsService userDetailsService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		try {
			String jwt = getJwt(request);
			logger1.info("---------------fiilter----------------");
			if(jwt!=null && jwtProvider.validateToken(jwt)) {
				String userName=jwtProvider.getUserNameFromJwtToken(jwt);
				UserDetails userDetails=userDetailsService.loadUserByUsername(userName);
				UsernamePasswordAuthenticationToken authentication=new UsernamePasswordAuthenticationToken(userDetails, null,userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		} catch (Exception e) {
			logger1.error("Can NOT set user authentication -> Message: {}", e);
		}
		filterChain.doFilter(request, response);
	}

	private String getJwt(HttpServletRequest request) {
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null && authHeader.startsWith("Bearer ")) {
			return authHeader.replace("Bearer ", "");
		}
		return null;
	}
}