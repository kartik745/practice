package com.kartik.Book;

public class StudentRegistration {
	private int studentId;
	private String studentName;
	private String department;

	public StudentRegistration() {
		super();
	}

	public StudentRegistration(int studentId, String studentName, String department) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.department = department;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "StudentRegistration [studentId=" + studentId + ", studentName=" + studentName + ", department="
				+ department + "]";
	}

}
