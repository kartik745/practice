package com.kartik.Book;

public class ReturnBook {
	private int returnId;
	private IssueBook issueBook;
	private String returnDate;

	@Override
	public String toString() {
		return "ReturnBook [returnId=" + returnId + ", issueBook=" + issueBook + ", returnDate=" + returnDate + "]";
	}

	public ReturnBook() {
		super();
	}

	public ReturnBook(int returnId, StudentRegistration studentId, Book bookId, String returnDate) {
		super();
		this.returnId = returnId;

		this.returnDate = returnDate;
	}

	public IssueBook getIssueBook() {
		return issueBook;
	}

	public void setIssueBook(IssueBook issueBook) {
		this.issueBook = issueBook;
	}

	public int getReturnId() {
		return returnId;
	}

	public void setReturnId(int returnId) {
		this.returnId = returnId;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

}
