package com.kartik.Book;

import java.util.Scanner;

public class IssueBook {
	private int issueID;
	private StudentRegistration student;
	private Book book;
	private String issueDate;
	Scanner sc = new Scanner(System.in);

	public IssueBook() {
		super();
	}

	public IssueBook(int issueId, StudentRegistration student, Book book, String issueDate) {
		super();
		this.issueID = issueId;
		this.student = student;
		this.book = book;
		this.issueDate = issueDate;
	}

	public int getIssueId() {
		return issueID;
	}

	public void setIssueId(int issueId) {
		this.issueID = issueId;
	}

	public StudentRegistration getStudent() {
		return student;
	}

	public void setStudent(StudentRegistration student) {
		this.student = student;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	@Override
	public String toString() {
		return "IssueBook [issueID=" + issueID + ", student=" + student + ", book=" + book + ", issueDate=" + issueDate
				+ "]";
	}

}
