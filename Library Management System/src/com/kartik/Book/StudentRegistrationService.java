package com.kartik.Book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class StudentRegistrationService {
	private static int issueBookIdCounter = 1;
	private StudentRegistration studentRegistration;
	BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));

	@Override
	public String toString() {
		return "StudentRegistrationService [studentRegistration=" + studentRegistration + "]";
	}

	public StudentRegistration getStudentRegistration() {
		return studentRegistration;
	}

	public void setStudentRegistration(StudentRegistration studentRegistration) {
		this.studentRegistration = studentRegistration;
	}

	public StudentRegistrationService searchStudentById(int studentId, List<StudentRegistrationService> studentList) {
		for (StudentRegistrationService s : studentList) {
			if (s.getStudentRegistration().getStudentId() == studentId)
				return s;

		}
		//System.out.println("student Id is not present");
		return null;
	}

	public StudentRegistrationService addStudentDetails() throws IOException {
		StudentRegistration tempAddStudent = new StudentRegistration();
		int studentId = issueBookIdCounter;
		issueBookIdCounter++;
		tempAddStudent.setStudentId(studentId);
		System.out.println("Enter YourName:");
		String studentName = sc.readLine();
		tempAddStudent.setStudentName(studentName);
		System.out.println("Enter Department:");
		String department = sc.readLine();
		tempAddStudent.setDepartment(department);
		setStudentRegistration(tempAddStudent);
		StudentRegistrationService tempStudentService = new StudentRegistrationService();
		tempStudentService.setStudentRegistration(tempAddStudent);
		return tempStudentService;
	}

}
