package com.kartik.Book;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ReturnBookService {
	private static int returnBookIdCounter = 1;
	BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
	private ReturnBook returnBook;

	@Override
	public String toString() {
		return "ReturnBookService [returnBook=" + returnBook + "]";
	}

	public ReturnBook getReturnBook() {
		return returnBook;
	}

	public void setReturnBook(ReturnBook returnBook) {
		this.returnBook = returnBook;
	}

	public boolean checkForDate(Date issueDateCompare1, Date issueDateCompare2) {
		int diffdays = (int) ((issueDateCompare2.getTime() - issueDateCompare1.getTime()) / (1000 * 60 * 60 * 24));
		System.out.println("diff of days" + diffdays);
		if (diffdays > 15) {
			return false;
		} else
			return true;

	}

	public ReturnBookService addReturnBookDetails(List<IssueBookService> issueList) {
		ReturnBook tempReturnBook = new ReturnBook();

		int returnId = returnBookIdCounter;
		returnBookIdCounter++;
		tempReturnBook.setReturnId(returnId);
		try {
			System.out.println("Enter IssueId");
			int issueId = Integer.parseInt(sc.readLine());
			IssueBookService tempIssueBook1 = new IssueBookService();
			IssueBookService tempIssueBook2 = tempIssueBook1.searchBookById(issueId, issueList);

			if (tempIssueBook2 != null)
				tempReturnBook.setIssueBook(tempIssueBook2.getIssueBook());
			else
				return null;

			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();
			String returnDate = formatter.format(date);
			tempReturnBook.setReturnDate(returnDate);

			Date issueDateCompare1 = formatter.parse(tempIssueBook2.getIssueBook().getIssueDate());
			Date issueDateCompare2 = formatter.parse(returnDate);
			// System.out.println(tempIssueBook2);
			if (checkForDate(issueDateCompare1, issueDateCompare2)) {
				ReturnBookService tempReturnBookService = new ReturnBookService();
				tempReturnBookService.setReturnBook(tempReturnBook);
				// System.out.println("check for return book object"+tempReturnBookService);
				return tempReturnBookService;
			} else {
				ReturnBookService tempReturnBookService = new ReturnBookService();
				tempReturnBookService.setReturnBook(tempReturnBook);
				System.out.println("you have to pay penalty for that");
				return tempReturnBookService;
			}
		} catch (NumberFormatException e) {
			System.out.println("enter valid input ");
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
		return null;

	}
}
