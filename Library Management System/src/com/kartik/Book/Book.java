package com.kartik.Book;

public class Book {

	private int bookId;
	private String bookName;
	private String Author;
	private String Department;

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookName=" + bookName + ", Author=" + Author + ", Department=" + Department
				+ "]";
	}

	public Book() {
		super();
	}

	public String getAuthor() {
		return Author;
	}

	public void setAuthor(String author) {
		Author = author;
	}

	public Book(int bookId, String bookName, String author, String department) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		Author = author;
		Department = department;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		Department = department;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

}
