package com.kartik.Book;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainClass {

	public static void main(String[] args) throws Exception {
		List<StudentRegistrationService> studentList = new ArrayList<>();
		List<BookService> bookList = new ArrayList<>();
		List<IssueBookService> issueList = new ArrayList<>();
		int choice;
		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		boolean check = true;

		while (check) {
			System.out.println("choose operations");
			System.out.println("press 1 for Student Registration");
			System.out.println("press 2 for Add Book");
			System.out.println("press 3 for Search Book");
			System.out.println("press 4 for Issue Book");
			System.out.println("press 5 for Return Book");
			System.out.println("press 6 for View Books");
			System.out.println("press 7 for View Issued books");
			System.out.println("press 8 for exit");

			try {

				choice = Integer.parseInt(sc.readLine());
				switch (choice) {

				case 1:
					StudentRegistrationService student = new StudentRegistrationService();
					StudentRegistrationService tempStudent = student.addStudentDetails();
					if (tempStudent != null) {
						studentList.add(student);
						System.out.println("student list: " + studentList);
					}
					break;

				case 2:
					BookService bookService = new BookService();
					bookService.addBookDetails();
					bookList.add(bookService);
					System.out.println("book list: " + bookList);
					break;

				case 3:
					BookService searchBook = new BookService();
					BookService tempBook = searchBook.searchBookByName(bookList);
					if (tempBook != null)
						System.out.println(tempBook);
					break;

				case 4:
					IssueBookService issueBook = new IssueBookService();
					IssueBookService tempIssueBook = issueBook.addIssueDetails(bookList, studentList);
					if (tempIssueBook != null) {
						issueList.add(tempIssueBook);
						System.out.println("issueList :" + issueList);
					}
					break;

				case 5:
					ReturnBookService returnBook = new ReturnBookService();
					ReturnBookService tempReturnBook = returnBook.addReturnBookDetails(issueList);
					if (tempReturnBook != null)
						System.out.println("return book :" + tempReturnBook);
					break;

				case 6:
					if (bookList != null && !bookList.isEmpty())
						System.out.println(bookList);
					else
						System.out.println("book is not available");
					break;

				case 7:
					if (issueList != null && !issueList.isEmpty())
						System.out.println(issueList);
					else
						System.out.println("zero book issued");
					break;

				default:
					System.out.println("exit successfull");
					check = false;
					break;
				}
			} catch (NumberFormatException e) {

				System.out.println("Invalid input");
			}

		}
		sc.close();
	}

}