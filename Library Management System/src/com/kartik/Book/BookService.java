package com.kartik.Book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class BookService {
	private static int bookIdCounter = 1;
	BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
	private Book book;

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@Override
	public String toString() {
		return "BookService [book=" + book + "]";
	}

	public void addBookDetails() throws IOException {
		Book book1 = new Book();
		int bookId = bookIdCounter;
		bookIdCounter++;
		book1.setBookId(bookId);
		System.out.println("Enter BookName:");
		String bookName = sc.readLine();
		book1.setBookName(bookName);
		System.out.println("Enter Author");
		String author = sc.readLine();
		book1.setAuthor(author);
		System.out.println("Enter Department:");
		String department = sc.readLine();
		book1.setDepartment(department);
		setBook(book1);
	}

	public BookService searchBookByName(List<BookService> bookList) throws IOException {
		System.out.println("Enter Book Name you want to Search");
		String bookName = sc.readLine();

		for (BookService b : bookList) {
			if (b.getBook().getBookName().equalsIgnoreCase(bookName)) {
				System.out.println("book is available");
				return b;
			}
		}
		System.out.println("Book is not available");
		return null;
	}

	public BookService searchBookById(int bookId, List<BookService> bookList) {

		for (BookService b : bookList) {
			if (b.getBook().getBookId() == bookId) {
				System.out.println("book id exist" + b);
				return b;
			}

		}
		//System.out.println("book id is not exist ");
		return null;

	}
	
}
