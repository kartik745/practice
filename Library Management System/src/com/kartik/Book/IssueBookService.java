package com.kartik.Book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class IssueBookService {
	private static int issueBookIdCounter = 1;
	BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
	private IssueBook issueBook;

	@Override
	public String toString() {
		return "IssueBookService [issueBook=" + issueBook + "]";
	}

	public IssueBook getIssueBook() {
		return issueBook;
	}

	public void setIssueBook(IssueBook issueBook) {
		this.issueBook = issueBook;
	}

	public IssueBookService addIssueDetails(List<BookService> bookList, List<StudentRegistrationService> studentList) {
		IssueBook issueBookTemp = new IssueBook();

		int issueId = issueBookIdCounter;
		issueBookIdCounter++;
		issueBookTemp.setIssueId(issueId);
		try {
			System.out.println("Enter StudentId");
			int studentId = Integer.parseInt(sc.readLine());
			StudentRegistrationService getStudentByIdObj = new StudentRegistrationService();
			StudentRegistrationService tempRegisterStudent = getStudentByIdObj.searchStudentById(studentId, studentList);
			IssueBookService tempIssueBook = new IssueBookService();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();
			String issueDate = formatter.format(date);

			if (tempRegisterStudent != null) {
				System.out.println("Enter BookId");
				int bookId = Integer.parseInt(sc.readLine());
				BookService getBookById = new BookService();
				BookService tempBookObject = getBookById.searchBookById(bookId, bookList);
				if (tempBookObject != null) {
					issueBookTemp.setIssueDate(issueDate);
					issueBookTemp.setBook(tempBookObject.getBook());
					issueBookTemp.setStudent(tempRegisterStudent.getStudentRegistration());
					tempIssueBook.setIssueBook(issueBookTemp);
					return tempIssueBook;
				} else {
					System.out.println(" Book Id is not valid");
					return null;
				}
			} else {
				System.out.println("Student Id is not valid");
				return null;
			}
		} catch (NumberFormatException | IOException e) {
			System.out.println("enter valid input");

		} catch (Exception e) {
			System.out.println("exception :" + e);
		}

		return null;

	}

	public IssueBookService searchBookById(int issueId, List<IssueBookService> issueList) {

		for (IssueBookService i : issueList) {
			if (i.getIssueBook().getIssueId() == issueId) {
				System.out.println("issue id exist" + i);
				return i;
			}

		}
		System.out.println("issue id is not exist ");
		return null;

	}
}
